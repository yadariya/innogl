# Frontend Innogl
This folder contains frontend part of Innogl project. 


## How to run
### Pre-installation
You should have pre-installed [NodeJS](https://nodejs.org/en/download/)

### Running a project
Run the following commands in your terminal:
1. Move to the current folder
2. Install all required dependencies
```shell
npm i
```
3. Run the application
```shell
npm start
```

**Important**: do not forget to run back-end before using the app.

Now the application is available in your browser: [http://localhost:3000](http://localhost:3000)

## Available Scripts
In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.